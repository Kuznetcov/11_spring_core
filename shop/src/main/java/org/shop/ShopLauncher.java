package org.shop;

import org.shop.api.ProposalService;
import org.shop.configuration.UnitedConfiguration;
import org.shop.repository.OrderRepository;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * The ShopLauncher class.
 */
public class ShopLauncher {
    
    /**
     * The main method.
     *
     * @param args the arguments
     */
    public static void main(String[] args) {
      AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(UnitedConfiguration.class);
      ProposalService p = (ProposalService) applicationContext.getBean("proposalService");
      System.out.println(p.getProposalsBySellerId(1l));
      
      OrderRepository r = applicationContext.getBean(OrderRepository.class);
      System.out.println(r.getOrderById(1l));
      
      applicationContext.close();;
    }
}
