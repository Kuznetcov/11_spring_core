package org.shop.configuration;

import org.shop.api.*;
import org.shop.api.impl.*;
import org.shop.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(RepositoryConfiguration.class)
public class ServiceConfiguration {


  
  @Autowired
  private ProductRepository productRepository;
  
  @Autowired
  private ProposalRepository proposalRepository;
  
  @Autowired
  private UserRepository userRepository;
  
  @Bean
  public ItemService itemService(ItemRepository itemRepository){
    return new ItemServiceImpl(itemRepository);
  }
  
  @Bean
  public OrderService orderService(){
    return new OrderServiceImpl();
  }
  
  @Bean
  public ProductService productService(){
    return new ProductServiceImpl(productRepository);
  }
  
  
  @Bean
  public ProposalService proposalService(){
    return new ProposalServiceImpl(proposalRepository);
  }
  
  
  @Bean
  public SellerService sellerService(){
    return new SellerServiceImpl();
  }
  
  
  @Bean
  UserService userService(){
    UserServiceImpl userService = new UserServiceImpl();
    userService.populate(userRepository);
    return userService;
  }
  
}
