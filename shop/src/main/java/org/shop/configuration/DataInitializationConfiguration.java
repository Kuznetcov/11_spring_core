package org.shop.configuration;

import java.util.HashMap;
import java.util.Map;

import org.shop.DataInitializer;
import org.shop.ProductInitializer;
import org.shop.ProposalInitializer;
import org.shop.SellerInitializer;
import org.shop.UserInitializer;
import org.shop.api.ProductService;
import org.shop.api.UserService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DataInitializationConfiguration {
  
  
  @Bean (initMethod="initData")
  public DataInitializer dataInitializer(){
    return new DataInitializer();   
  }
  
  @Bean
  public ProductInitializer productInitializer(ProductService productService){
    return new ProductInitializer(productService);
  }
  
  @Bean
  public SellerInitializer sellerInitializer(){
    return new SellerInitializer();
  }
  
  @Bean
  public Map<Long,String> sellerNames(){
    Map<Long,String> t = new HashMap<Long, String>();
    t.put(1l, "seller1");
    t.put(2l, "seller2");
    return t;
  }
  
  @Bean
  public ProposalInitializer proposalInitializer(){
    return new ProposalInitializer();
  }
  
  @Bean
  public UserInitializer userInitializer(UserService userService){
    return new UserInitializer(userService);
  }
  
}
