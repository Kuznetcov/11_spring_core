package org.shop.configuration;

import org.shop.repository.*;
import org.shop.repository.factory.UserRepositoryFactory;
import org.shop.repository.map.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;

@Configuration
@PropertySource("classpath:/application.properties")
@Import(FactoryConfiguration.class)
public class RepositoryConfiguration {

  @Autowired
  Environment env;
  
  @Autowired
  private UserRepositoryFactory userRepositoryFactory;
  
  @Bean
  public ItemRepository itemRepository(){
    return new ItemMapRepository();
  }
  
  @Bean
  public OrderRepository orderRepository(){
     OrderMapRepository rep = new OrderMapRepository();
     rep.setSequence(Long.valueOf(env.getProperty("intitialSequence"))); 
     return rep;
  }
  
  @Bean
  public ProductRepository productRepository(){
    return new ProductMapRepository();
  }
  
  @Bean
  public ProposalRepository proposalRepository(){
    return new ProposalMapRepository();
  }
  
  @Bean
  public SellerRepository sellerRepository(){
    return new SellerMapRepository();
  }
  
  @Bean
  public UserRepository userRepository(){
    return userRepositoryFactory.createUserRepository();
  }
  
}
