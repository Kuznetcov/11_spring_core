package org.shop.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({RepositoryConfiguration.class,DataInitializationConfiguration.class,FactoryConfiguration.class,ServiceConfiguration.class})
public class UnitedConfiguration {

//  @Autowired
//  private RepositoryConfiguration repositoryConfiguration;
//  @Autowired
//  private DataInitializationConfiguration dataInitializationConfiguration;
//  @Autowired
//  private FactoryConfiguration factoryConfiguration;
//  @Autowired
//  private ServiceConfiguration serviceConfiguration;
//  
}
